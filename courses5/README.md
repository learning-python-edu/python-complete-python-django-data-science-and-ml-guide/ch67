# Chapter 67: Create a Django application

Django version 5.0.3.

## Create Django application

In previously created Django project execute following command:
```shell
python manage.py startapp shop
```

This command will create an new Django application named `shop` in the current Django project directory.
